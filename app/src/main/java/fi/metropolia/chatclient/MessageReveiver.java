package fi.metropolia.chatclient;

/**
 * Created by admin on 20/09/2015.
 */

import android.os.Message;

import java.net.*;
import java.io.*;
import java.util.logging.Handler;

public class MessageReveiver implements Runnable{

    private Socket socket;
    private BufferedReader in;
    private android.os.Handler handler;
    //private Handler handler;

    public MessageReveiver(Socket socket, android.os.Handler handler) {
        this.socket = socket;
        this.handler = handler;
    }

    @Override
    public void run() {

        try {
            socket = new Socket("10.112.214.84",12345);
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (Exception e) {
            System.out.println(e);
        }

        String input = "";
        try {
            if ((input = in.readLine()) != null) {
                Message msg = handler.obtainMessage();
                msg.what = 0;
                msg.obj = input;
                handler.sendMessage(msg);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public Socket getSocket() {
        return this.socket;
    }
}
