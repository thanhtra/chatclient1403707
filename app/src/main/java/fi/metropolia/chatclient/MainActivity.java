package fi.metropolia.chatclient;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.*;

public class MainActivity extends Activity implements View.OnClickListener{

    // Declare UI elements
    //private Button sendButton;
    private TextView viewMessage;
    private EditText editMessage;
    private Socket socket;

    private Handler uiHandler = new Handler() {
      public void handleMessage(Message msg) {
          if (msg.what == 0) {
              viewMessage.setText((String)msg.obj);
          }
      }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        socket = new Socket();
        Button sendButton = (Button)findViewById(R.id.send_button);
        viewMessage = (TextView) findViewById(R.id.view_message);
        editMessage = (EditText) findViewById(R.id.edit_message);

        MessageReveiver msgReceiver = new MessageReveiver(socket,uiHandler);
        Thread receiver = new Thread(msgReceiver);
        receiver.start();

        sendButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        String message = editMessage.getText().toString();
        viewMessage.setText("You: " + message);
        Thread sender = new Thread(new MessageSender(socket, message));
        sender.start();
    }








    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
