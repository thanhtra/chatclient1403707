package fi.metropolia.chatclient;

/**
 * Created by admin on 20/09/2015.
 */

import java.net.*;
import java.io.*;

public class MessageSender implements Runnable{

    private Socket socket;
    private PrintWriter out;
    private String output;

    public MessageSender(Socket socket, String message) {
        this.socket = socket;
        this.output = message;
    }

    @Override
    public void run() {


        try {
            socket = new Socket("10.112.214.84",12345);
            this.out = new PrintWriter(socket.getOutputStream(),true);
        } catch (Exception e) {
            System.out.println(e);
        }

        if (output != null) {
            this.out.println(output);
        }
    }
}
